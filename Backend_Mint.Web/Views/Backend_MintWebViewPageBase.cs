﻿using Abp.Web.Mvc.Views;

namespace Backend_Mint.Web.Views
{
    public abstract class Backend_MintWebViewPageBase : Backend_MintWebViewPageBase<dynamic>
    {

    }

    public abstract class Backend_MintWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected Backend_MintWebViewPageBase()
        {
            LocalizationSourceName = Backend_MintConsts.LocalizationSourceName;
        }
    }
}