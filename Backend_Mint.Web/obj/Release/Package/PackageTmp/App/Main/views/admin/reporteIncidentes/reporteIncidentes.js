﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.reporteIncidentes';

    /*****************************************************************
    * 
    * CONTROLADOR DE REPORTE DE INCIDENTES
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.listaReportesIncidente = [];

           //Funcion encargada de consultar las preguntas frecuentes en la base de datos
           function cargarReporteIncidentes() {
               adminService.getAllReporteIncidentes().success(function (data) {
                   vm.listaReportesIncidente = mint.tablas.paginar(data.reportesIncidentes, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarReporteIncidentes();

           /************************************************************************
           * Llamado para abrir Modal para Consultar detalles del reporte de incidentes
           ************************************************************************/
           vm.abrirModalConsultar = function (reporteId) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/reporteIncidentes/partials/modalConsultarReporteIncidentes.cshtml',
                   controller: 'modalConsultarReporteIncidentesController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       reporteEditar: function () {
                           return reporteId;
                       }
                   }
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar un reporte de incidente
           ************************************************************************/
           vm.abrirModalEliminar = function (reporteId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/reporteIncidentes/partials/modalEliminarReporteIncidentes.cshtml',
                    controller: 'modalEliminarReporteIncidentesController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        reporteInactivar: function () {
                            return reporteId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    cargarReporteIncidentes();
                    abp.notify.success('Se eliminó correctamente el reporte.', 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al eliminar el reporte.'
                });
           }
       }]);
})();