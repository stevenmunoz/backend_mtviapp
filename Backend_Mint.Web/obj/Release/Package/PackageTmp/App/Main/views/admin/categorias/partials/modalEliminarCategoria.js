﻿(function () {
    angular.module('app').controller('modalEliminarCategoriaController', ['$scope', '$modalInstance', 'categoriaEliminar', 'abp.services.app.admin',
        function ($scope, $modalInstance, categoriaEliminar, adminService) {

            adminService.getTipoReporte({ id: categoriaEliminar })
                .success(function (data) {
                    $scope.categoria = data;
                });

            $scope.okModal = function () {
                adminService.deleteTipo({ id: categoriaEliminar })
                    .success(function () {
                        $modalInstance.close($scope.categoria.nombre);
                    }).error(function (error) {
                       
                        $scope.mensajeError = error.message;
                    });
            }


            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();

