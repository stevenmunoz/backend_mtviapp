﻿(function () {
    angular.module('app').controller('modalEliminarDiagnosticoController', ['$scope', '$modalInstance', 'diagnosticoEliminar', 'abp.services.app.admin',
        function ($scope, $modalInstance, diagnosticoEliminar, adminService) {

            adminService.getItemByDiagnosticoVial({ id: diagnosticoEliminar })
                .success(function (data) {
                    $scope.diagnosticoEliminar = data;
                });

            $scope.okModal = function () {
                adminService.deleteItemDiagnosticoVial({ id: diagnosticoEliminar })
                    .success(function () {
                        $modalInstance.close($scope.diagnosticoEliminar.nombre);
                    }).error(function (error) {
                       
                        $scope.mensajeError = error.message;
                    });
            }


            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();

