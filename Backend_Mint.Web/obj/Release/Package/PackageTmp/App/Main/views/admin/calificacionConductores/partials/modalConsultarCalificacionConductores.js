﻿(function () {
    angular.module('app').controller('modalConsultarCalificacionConductoresController', ['$scope', '$modalInstance', 'reporteEditar', 'abp.services.app.admin',
        function ($scope, $modalInstance, reporteEditar, adminService) {

            $scope.reporteCalificacion = '';

            adminService.getReporteCalificaciones({ id: reporteEditar })
                .success(function (data) {
                    $scope.reporteCalificacion = data;
                });

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();