﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.categorias';

    /*****************************************************************
    * 
    * CONTROLADOR DE CATEGORIAS
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.categorias = [];

           //Funcion encargada de consultar las categorias en la base de datos
           function cargarCategorias() {
               adminService.getAllTipos().success(function (data) {
                   vm.categorias = mint.tablas.paginar(data.tiposReporte, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarCategorias();

           /************************************************************************
            * Llamado para abrir Modal para Nueva Categoria
            ************************************************************************/

           vm.abrirModalNueva= function () {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/categorias/partials/modalNuevaCategoria.cshtml',
                   controller: 'modalNuevaCategoriaController',
                   size: 'md',
                   animation: false
               });

               modalInstance.result.then(function (categoria) {
                   cargarCategorias();
                   abp.notify.success('Se guardó correctamente la categoría: ' + categoria, 'Información');
               }, function () {
                   vm.resultado = 'Ocurrió un problema al guardar la categoría'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Editar Categoría
           ************************************************************************/
           vm.abrirModalEditar = function (categoriaId) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/categorias/partials/modalEditarCategoria.cshtml',
                   controller: 'modalEditarCategoriaController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       categoriaEditar: function () {
                           return categoriaId;
                       }
                   }
               });

               modalInstance.result.then(function (categoria) {
                   abp.notify.success('Se actualizó correctamente la categoría: ' + categoria, 'Información');
                   cargarCategorias();
               }, function () {
                   vm.resultado = 'Ocurrió un problema al actualizar la categoría'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar la categoría
           ************************************************************************/
           vm.abrirModalEliminar = function (categoriaId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/categorias/partials/modalEliminarCategoria.cshtml',
                    controller: 'modalEliminarCategoriaController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        categoriaEliminar: function () {
                            return categoriaId;
                        }
                    }
                });

                modalInstance.result.then(function (categoria) {
                    cargarCategorias();
                    abp.notify.success('Se eliminó correctamente la categoría: ' + categoria, 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al actualizar la categoría'
                });
           }
       }]);
})();