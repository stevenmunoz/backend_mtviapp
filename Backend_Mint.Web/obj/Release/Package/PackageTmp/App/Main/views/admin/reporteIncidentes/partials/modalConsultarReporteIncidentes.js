﻿(function () {
    angular.module('app').controller('modalConsultarReporteIncidentesController', ['$scope', '$modalInstance', 'reporteEditar', 'abp.services.app.admin',
        function ($scope, $modalInstance, reporteEditar, adminService) {

            $scope.reporteIncidente = '';

            adminService.getReporteIncidentes({ id: reporteEditar })
                .success(function (data) {
                    $scope.reporteIncidente = data;
                });

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();