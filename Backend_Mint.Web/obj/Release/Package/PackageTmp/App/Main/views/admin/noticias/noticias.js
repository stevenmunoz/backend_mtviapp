﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.noticias';

    /*****************************************************************
    * 
    * CONTROLADOR DE PREGUNTAS FRECUENTES
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.noticias = [];

           //Funcion encargada de consultar las noticias en la base de datos
           function cargarNoticias() {
               adminService.getAllNoticias().success(function (data) {
                   vm.noticias = mint.tablas.paginar(data.noticias, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarNoticias();

           /************************************************************************
            * Llamado para abrir Modal para Nueva Noticia
            ************************************************************************/

           vm.abrirModalNueva= function () {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/noticias/partials/modalNuevoNoticias.cshtml',
                   controller: 'modalNuevoNoticiaController',
                   size: 'md',
                   animation: false
               });

               modalInstance.result.then(function (noticia) {
                   cargarNoticias();
                   abp.notify.success('Se guardó correctamente la noticia: ' + noticia, 'Información');
               }, function () {
                   vm.resultado = 'Ocurrió un problema al guardar la noticia';
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Editar noticia
           ************************************************************************/
           vm.abrirModalEditar = function (noticiaid) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/noticias/partials/modalEditarNoticias.cshtml',
                   controller: 'modalEditarNoticiaController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       noticiaEditar: function () {
                           return noticiaid;
                       }
                   }
               });

               modalInstance.result.then(function (noticia) {
                   abp.notify.success('Se actualizó correctamente la noticia: ' + noticia, 'Información');
                   cargarNoticias();
               }, function () {
                   vm.resultado = 'Ocurrió un problema al actualizar la noticia '
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar noticia
           ************************************************************************/
           vm.abrirModalEliminar = function (noticiaId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/noticias/partials/modalEliminarNoticias.cshtml',
                    controller: 'modalEliminarNoticiaController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        noticiaEliminar: function () {
                            return noticiaId;
                        }
                    }
                });

                modalInstance.result.then(function (noticia) {
                    cargarNoticias();
                    abp.notify.success('Se eliminó correctamente la noticia: ' + noticia, 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al actualizar la noticia '
                });
           }

           /************************************************************************
           * Llamado para modificar el estado de la noticia
           ************************************************************************/
           vm.modificarEstadoNoticia = function (noticia) {
               if (noticia.esActiva) {
                   noticia.esActiva = false;
               } else {
                   noticia.esActiva = true;
               }
               adminService.updateNoticias(noticia).success(function () {
                       abp.notify.success('Se modificó correctamente el estado de la noticia: ' + noticia.titulo, abp.localization.localize('', 'Bow') + 'Información');
                       cargarNoticias();
                   }).error(function (error) {
                       $scope.mensajeError = error.message;
                   });
           }
       }]);
})();