﻿using Abp.Auditing;
using Abp.Configuration.Startup;
using Abp.UI;
using Abp.Web.Mvc.Models;
using Backend_Mint.Users;
using Backend_Mint.Users.DTOs.InputModels;
using Backend_Mint.Web.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Backend_Mint.Web.Controllers
{
    public class AccountController : Backend_MintControllerBase
    {
        private readonly UserService _userService;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountController(UserService userService)
        {
            _userService = userService;
        }

        public ActionResult Login(string returnUrl = "")
        {
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Request.ApplicationPath;
            }

            return View(
                new LoginFormViewModel
                {
                    ReturnUrl = returnUrl,
                    IsMultiTenancyEnabled = false
                });
        }

        [HttpPost]
        public async Task<JsonResult> Login(LoginViewModel loginModel, string returnUrl = "")
        {
            if (string.IsNullOrEmpty(loginModel.UsernameOrEmailAddress) || string.IsNullOrEmpty(loginModel.Password))
            {
                throw new UserFriendlyException("Los datos son incorrectos.");
            }

            var loginResult = _userService.GetLoginUser(new GetLoginUserInput() { User = loginModel.UsernameOrEmailAddress, Password = loginModel.Password });

            switch (loginResult.Result)
            {
                case true:
                    Session.Add("userLogged", true);
                    break;
                case false:
                    throw new UserFriendlyException("Usuario o contraseña inválidos");
                default: //Can not fall to default for now. But other result types can be added in the future and we may forget to handle it
                    throw new UserFriendlyException("Ocurrió un error inesperado");
            }

            //var claimsIdentity = new ClaimsIdentity(new[] {
            //    new Claim(ClaimTypes.Name, "Ben"),
            //    new Claim(ClaimTypes.Email, "a@b.com"),
            //    new Claim(ClaimTypes.Country, "England")
            //},
            //"ApplicationCookie");

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            //AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = loginModel.RememberMe }, claimsIdentity);

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Request.ApplicationPath;
            }

            return Json(new MvcAjaxResponse { TargetUrl = returnUrl });
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            Session.Remove("userLogged");
            return RedirectToAction("Login");
        }
    }
}