﻿using Abp.Web.Mvc.Controllers;

namespace Backend_Mint.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class Backend_MintControllerBase : AbpController
    {
        protected Backend_MintControllerBase()
        {
            LocalizationSourceName = Backend_MintConsts.LocalizationSourceName;
        }
    }
}