﻿using Abp.Web.Mvc.Authorization;
using System.Web.Mvc;

namespace Backend_Mint.Web.Controllers
{
    //[AbpMvcAuthorize]
    public class HomeController : Backend_MintControllerBase
    {
        public ActionResult Index()
        {
            if (Session["userLogged"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
            }
        }
	}
}