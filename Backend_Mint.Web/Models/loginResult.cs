﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend_Mint.Web.Controllers
{
    public class LoginResult
    {
        public string Result { get; set; }
        public string Identity { get; set; }
    }
}
