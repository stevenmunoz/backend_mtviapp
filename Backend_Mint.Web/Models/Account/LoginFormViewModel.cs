﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend_Mint.Web.Models.Account
{
    public class LoginFormViewModel
    {
        public string ReturnUrl { get; set; }

        public bool IsMultiTenancyEnabled { get; set; }
    }
}