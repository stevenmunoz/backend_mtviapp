﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.calificacionConductores';

    /*****************************************************************
    * 
    * CONTROLADOR DE CALIFICACION DE CONDUCTORES
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.listaReporteConductores = [];

           //Funcion encargada de consultar las preguntas frecuentes en la base de datos
           function cargarReporteConductores() {
               adminService.getAllReporteCalificaciones().success(function (data) {
                   vm.listaReporteConductores = mint.tablas.paginar(data.reportesCalificaciones, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarReporteConductores();

           /************************************************************************
           * Llamado para abrir Modal para consultar detalle del reporte
           ************************************************************************/
           vm.abrirModalConsultar = function (reporteId) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/calificacionConductores/partials/modalConsultarCalificacionConductores.cshtml',
                   controller: 'modalConsultarCalificacionConductoresController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       reporteEditar: function () {
                           return reporteId;
                       }
                   }
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Inactivar el reporte de calificación
           ************************************************************************/
           vm.abrirModalEliminar = function (reporteId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/calificacionConductores/partials/modalEliminarCalificacionConductores.cshtml',
                    controller: 'modalEliminarCalificacionConductoresController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        reporteInactivar: function () {
                            return reporteId;
                        }
                    }
                });

                modalInstance.result.then(function (pregunta) {
                    cargarPreguntasFrecuentes();
                    abp.notify.success('Se inactivó correctamente el reporte.', 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al modificar el reporte'
                });
           }

       }]);
})();