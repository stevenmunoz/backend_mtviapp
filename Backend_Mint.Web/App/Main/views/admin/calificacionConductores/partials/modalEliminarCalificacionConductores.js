﻿(function () {
    angular.module('app').controller('modalEliminarCalificacionConductoresController', ['$scope', '$modalInstance', 'reporteInactivar', 'abp.services.app.admin',
        function ($scope, $modalInstance, reporteInactivar, adminService) {

            adminService.getPreguntaFrecuente({ id: reporteInactivar })
                .success(function (data) {
                    $scope.preguntaFrecuente = data;
                });

            $scope.okModal = function () {
                adminService.deletePreguntaFrecuente({ id: reporteInactivar })
                    .success(function () {
                        $modalInstance.close($scope.preguntaFrecuente.pregunta);
                    }).error(function (error) {
                       
                        $scope.mensajeError = error.message;
                    });
            }


            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();

