﻿(function () {
    angular.module('app').controller('modalEditarHistoriaVialController', ['$scope', '$modalInstance', 'historiaEditar', 'abp.services.app.admin',
        function ($scope, $modalInstance, historiaEditar, adminService) {

            $scope.codigoHistoria = historiaEditar;

            $scope.historiaVial = {
                id: '',
                nombre: '',
                descripcion: '',
                nombrePersona: '',
                url: '',
                categoriaId: '',
                categoria: '',
                esActiva: true
            };

            adminService.getHistoriaVial({ id: historiaEditar })
                .success(function (data) {
                    console.log(data);
                    $scope.historiaVial = data;
                });

            $scope.okModal = function () {
                adminService.updateHistoriasVial($scope.historiaVial)
                    .success(function () {
                        $modalInstance.close($scope.historiaVial.nombre);
                    }).error(function (error) {
                        $scope.mensajeError = error.message;
                    });
            }

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();