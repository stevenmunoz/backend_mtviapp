﻿(function () {
    angular.module('app').controller('modalNuevaHistoriaVialController', ['$scope', '$modalInstance', 'abp.services.app.admin',
        function ($scope, $modalInstance, adminService) {

            $scope.historiaVial = {
                nombre: '',
                descripcion: '',
                nombrePersona: '',
                url: '',
                categoriaId: ''
            };

            $scope.listaCategorias = '';

            //Funcion encargada de consultar las categorias disponibles
            function cargarCategorias() {
                adminService.getAllCategorias().success(function (data) {
                    $scope.listaCategorias = data.tiposReporte;
                }).error(function (error) {
                    console.log(error);
                });
            }
            cargarCategorias();

            $scope.okModal = function () {
                adminService.saveHistoriasVial($scope.historiaVial)
                    .success(function () {
                        $modalInstance.close($scope.historiaVial.nombre);
                    }).error(function (error) {
                        $scope.mensajeError = error.message;
                    });
            }

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();