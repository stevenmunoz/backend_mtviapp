﻿(function () {
    angular.module('app').controller('modalEliminarHistoriaVialController', ['$scope', '$modalInstance', 'historiaEliminar', 'abp.services.app.admin',
        function ($scope, $modalInstance, historiaEliminar, adminService) {

            adminService.getHistoriaVial({ id: historiaEliminar })
                .success(function (data) {
                    $scope.historiaVial = data;
                });

            $scope.okModal = function () {
                adminService.deleteHistoriasVial({ id: historiaEliminar })
                    .success(function () {
                        $modalInstance.close($scope.historiaVial.nombre);
                    }).error(function (error) {
                       
                        $scope.mensajeError = error.message;
                    });
            }


            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();

