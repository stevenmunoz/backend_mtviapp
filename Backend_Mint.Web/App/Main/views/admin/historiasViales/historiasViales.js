﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.historiasViales';

    /*****************************************************************
    * 
    * CONTROLADOR DE HISTORIAS EN LA VÍA
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.historiasViales = [];

           //Funcion encargada de consultar las historias en la base de datos
           function cargarHistoriasViales() {
               adminService.getAllHistoriasViales().success(function (data) {
                   vm.historiasViales = mint.tablas.paginar(data.historiasViales, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarHistoriasViales();

           /************************************************************************
            * Llamado para abrir Modal para Nueva Historia en la Vía
            ************************************************************************/

           vm.abrirModalNueva= function () {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/historiasViales/partials/modalNuevaHistoriaVial.cshtml',
                   controller: 'modalNuevaHistoriaVialController',
                   size: 'md',
                   animation: false
               });

               modalInstance.result.then(function (historia) {
                   cargarHistoriasViales();
                   abp.notify.success('Se guardó correctamente la historia: ' + historia, 'Información');
               }, function () {
                   vm.resultado = 'Ocurrió un problema al guardar la hisotoria'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Editar una Historia
           ************************************************************************/
           vm.abrirModalEditar = function (historiaId) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/historiasViales/partials/modalEditarHistoriaVial.cshtml',
                   controller: 'modalEditarHistoriaVialController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       historiaEditar: function () {
                           return historiaId;
                       }
                   }
               });

               modalInstance.result.then(function (historia) {
                   abp.notify.success('Se actualizó correctamente la historia: ' + historia, 'Información');
                   cargarHistoriasViales();

               }, function () {
                   vm.resultado = 'Ocurrió un problema al actualizar la historia vial'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar una Historia
           ************************************************************************/
           vm.abrirModalEliminar = function (historiaId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/historiasViales/partials/modalEliminarHistoriaVial.cshtml',
                    controller: 'modalEliminarHistoriaVialController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        historiaEliminar: function () {
                            return historiaId;
                        }
                    }
                });

                modalInstance.result.then(function (historia) {
                    cargarHistoriasViales();
                    abp.notify.success('Se eliminó correctamente la historia: ' + historia, 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al actualizar la historia vial'
                });
           }

           /************************************************************************
           * Llamado para modificar el estado de la historia vial
           ************************************************************************/
           vm.modificarEstadoHistoria = function (historia) {
               if (historia.esActiva) {
                   historia.esActiva = false;
               } else {
                   historia.esActiva = true;
               }
               adminService.updateHistoriasVial(historia)
                   .success(function () {
                       abp.notify.success('Se modificó correctamente el estado de la historia: ' + historia.nombre, abp.localization.localize('', 'Bow') + 'Información');
                       cargarHistoriasViales();
                   }).error(function (error) {
                       $scope.mensajeError = error.message;
                   });
           }
       }]);
})();