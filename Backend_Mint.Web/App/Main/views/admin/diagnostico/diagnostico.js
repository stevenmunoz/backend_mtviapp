﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.diagnostico';

    /*****************************************************************
    * 
    * CONTROLADOR DE PREGUNTAS DIAGNOSTICO
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.diagnostico = [];

           //Funcion encargada de consultar los diagnostico en la base de datos
       
           function cargarDiagnostico() {
               adminService.getAllItemsDiagnosticoVial().success(function (data) {
                   vm.diagnostico = mint.tablas.paginar(data.itemsDiagnosticoVial, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarDiagnostico();

           /************************************************************************
            * Llamado para abrir Modal para Nuevo Diagnostico
            ************************************************************************/

           vm.abrirModalNueva= function () {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/diagnostico/partials/modalNuevoDiagnostico.cshtml',
                   controller: 'modalNuevoDiagnosticoController',
                   size: 'md',
                   animation: false
               });

               modalInstance.result.then(function (diagnostico) {
                   cargarDiagnostico();
                   abp.notify.success('Se guardó correctamente el diagnostico: ' + diagnostico, 'Información');
               }, function () {
                   vm.resultado = 'Ocurrió un problema al guardar el diagnostico'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Editar Diagnostico
           ************************************************************************/
           vm.abrirModalEditar = function (diagnostico) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/diagnostico/partials/modalEditarDiagnostico.cshtml',
                   controller: 'modalEditarDiagnosticoController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       diagnosticoEditar: function () {
                           return diagnostico;
                       }
                   }
               });

               modalInstance.result.then(function (diagnostico) {
                   abp.notify.success('Se actualizó correctamente el diagnostico: ' + diagnostico, 'Información');
                   cargarDiagnostico();
               }, function () {
                   vm.resultado = 'Ocurrió un problema al actualizar el diagnostico '
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar diagnostico
           ************************************************************************/
           vm.abrirModalEliminar = function (diagnosticoId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/diagnostico/partials/modalEliminarDiagnostico.cshtml',
                    controller: 'modalEliminarDiagnosticoController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        diagnosticoEliminar: function () {
                            return diagnosticoId;
                        }
                    }
                });

                modalInstance.result.then(function (diagnostico) {
                    cargarDiagnostico();
                    abp.notify.success('Se eliminó correctamente el diagnostico: ' + diagnostico, 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al actualizar el diagnostico '
                });
           }

           /************************************************************************
           * Llamado para modificar el estado de la diagnostico
           ************************************************************************/
           vm.modificarEstadoDiagnostico = function (diagnostico) {
               if (diagnostico.esActivo) {
                   diagnostico.esActivo = false;
               } else {
                   diagnostico.esActivo = true;
               }
               adminService.updateItemDiagnosticoVial(diagnostico).success(function () {
                   abp.notify.success('Se modificó correctamente el estado del diagnostico: ' + diagnostico.nombre, abp.localization.localize('', 'Bow') + 'Información');
                   cargarDiagnostico();
                   }).error(function (error) {
                       $scope.mensajeError = error.message;
                   });
           }
       }]);
})();