﻿(function () {
    angular.module('app').controller('modalEliminarNoticiaController', ['$scope', '$modalInstance', 'noticiaEliminar', 'abp.services.app.admin',
        function ($scope, $modalInstance, noticiaEliminar, adminService) {

            adminService.getNoticias({ id: noticiaEliminar })
                .success(function (data) {
                    $scope.noticiaEliminar = data;
                });

            $scope.okModal = function () {
                adminService.deleteNoticias({ id: noticiaEliminar })
                    .success(function () {
                        $modalInstance.close($scope.noticiaEliminar.titulo);
                    }).error(function (error) {
                       
                        $scope.mensajeError = error.message;
                    });
            }


            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();

