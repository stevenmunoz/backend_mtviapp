﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.deslizador';

    /*****************************************************************
    * 
    * CONTROLADOR DE DESLIZADOR
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.deslizador = [];

           //Funcion encargada de consultar las imagenes del slider
           function cargarDeslizador() {
               adminService.getAllDeslizador().success(function (data) {
                   vm.deslizador = mint.tablas.paginar(data.deslizador, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarDeslizador();

           /************************************************************************
            * Llamado para abrir Modal para Nueva Imagen del slider
            ************************************************************************/

           vm.abrirModalNueva= function () {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/deslizador/partials/modalNuevaDeslizador.cshtml',
                   controller: 'modalNuevoDeslizadorController',
                   size: 'md',
                   animation: false
               });

               modalInstance.result.then(function (deslizador) {
                   cargarDeslizador();
                   abp.notify.success('Se guardó correctamente la imagen nueva del slider: ' + deslizador, 'Información');
               }, function () {
                   vm.resultado = 'Ocurrió un problema al guardar la imagen nueva del slider'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Imagen del slider
           ************************************************************************/
           vm.abrirModalEditar = function (delizadorId) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/deslizador/partials/modalEditarDeslizador.cshtml',
                   controller: 'modalEditarDeslizadorController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       deslizadorEditar: function () {
                           return delizadorId;
                       }
                   }
               });

               modalInstance.result.then(function (deslizador) {
                   abp.notify.success('Se actualizó correctamente la imagen nueva del slider: ' + deslizador, 'Información');
                   cargarDeslizador();

               }, function () {
                   vm.resultado = 'Ocurrió un problema al actualizar la imagen nueva del slider'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar una Imagen del slider
           ************************************************************************/
           vm.abrirModalEliminar = function (delizadorId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/deslizador/partials/modalEliminarDeslizador.cshtml',
                    controller: 'modalEliminarDeslizadorController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        deslizadorEliminar: function () {
                            return delizadorId;
                        }
                    }
                });

                modalInstance.result.then(function (deslizador) {
                    cargarDeslizador();
                    abp.notify.success('Se eliminó correctamente la imagen nueva del slider: ' + deslizador, 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al actualizar la imagen nueva del slider'
                });
           }

       }]);
})();