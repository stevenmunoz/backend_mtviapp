﻿(function () {
    //Nombre del controlador   
    var controllerId = 'app.views.admin.preguntasFrecuentes';

    /*****************************************************************
    * 
    * CONTROLADOR DE PREGUNTAS FRECUENTES
    * 
    *****************************************************************/

    angular.module('app').controller(controllerId, ['$scope', '$modal', 'abp.services.app.admin',
       function ($scope, $modal, adminService) {
           var vm = this;

           //Inicializando Modelos

           vm.preguntasFrecuentes = [];

           //Funcion encargada de consultar las preguntas frecuentes en la base de datos
           function cargarPreguntasFrecuentes() {
               adminService.getAllPreguntasFrecuentes().success(function (data) {
                   vm.preguntasFrecuentes = mint.tablas.paginar(data.preguntasFrecuentes, 10);
               }).error(function (error) {
                   console.log(error);
               });
           }
           cargarPreguntasFrecuentes();

           /************************************************************************
            * Llamado para abrir Modal para Nueva Pregunta Frecuente
            ************************************************************************/

           vm.abrirModalNueva= function () {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/preguntasFrecuentes/partials/modalNuevoPreguntasFrecuentes.cshtml',
                   controller: 'modalNuevoPreguntasFrecuentesController',
                   size: 'md',
                   animation: false
               });

               modalInstance.result.then(function (pregunta) {
                   cargarPreguntasFrecuentes();
                   abp.notify.success('Se guardó correctamente la pregunta: ' + pregunta, 'Información');
               }, function () {
                   vm.resultado = 'Ocurrió un problema al guardar la pregunta frecuente';
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Editar Pregunta Frecuente
           ************************************************************************/
           vm.abrirModalEditar = function (preguntaId) {
               var modalInstance = $modal.open({
                   templateUrl: '/App/Main/views/admin/preguntasFrecuentes/partials/modalEditarPreguntasFrecuentes.cshtml',
                   controller: 'modalEditarPreguntasFrecuentesController',
                   size: 'md',
                   animation: false,
                   resolve: {
                       preguntaEditar: function () {
                           return preguntaId;
                       }
                   }
               });

               modalInstance.result.then(function (pregunta) {
                   abp.notify.success('Se actualizó correctamente la pregunta: ' + pregunta, 'Información');
                   cargarPreguntasFrecuentes();

               }, function () {
                   vm.resultado = 'Ocurrió un problema al actualizar la pregunta frecuente'
               });
           }

           /************************************************************************
           * Llamado para abrir Modal para Eliminar Pregunta Frecuente
           ************************************************************************/
           vm.abrirModalEliminar = function (preguntaId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/admin/preguntasFrecuentes/partials/modalEliminarPreguntasFrecuentes.cshtml',
                    controller: 'modalEliminarPreguntasFrecuentesController',
                    size: 'md',
                    animation: false,
                    resolve: {
                        preguntaEliminar: function () {
                            return preguntaId;
                        }
                    }
                });

                modalInstance.result.then(function (pregunta) {
                    cargarPreguntasFrecuentes();
                    abp.notify.success('Se eliminó correctamente la pregunta: ' + pregunta, 'Información');
                }, function () {
                    vm.resultado = 'Ocurrió un problema al actualizar la pregunta frecuente'
                });
           }

           /************************************************************************
           * Llamado para modificar el estado de la pregunta frecuente
           ************************************************************************/
           vm.modificarEstadoPregunta = function (pregunta) {
               if (pregunta.esActiva) {
                   pregunta.esActiva = false;
               } else {
                   pregunta.esActiva = true;
               }
               adminService.updatePreguntaFrecuente(pregunta)
                   .success(function () {
                       abp.notify.success('Se modificó correctamente el estado de la pregunta: ' + pregunta.pregunta, 'Información');
                       cargarPreguntasFrecuentes();
                   }).error(function (error) {
                       $scope.mensajeError = error.message;
                   });
           }
       }]);
})();