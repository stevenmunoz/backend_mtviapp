﻿(function () {
    angular.module('app').controller('modalNuevoPreguntasFrecuentesController', ['$scope', '$modalInstance', 'abp.services.app.admin',
        function ($scope, $modalInstance, adminService) {

            $scope.preguntaFrecuente = {
                pregunta: '',
                respuesta: '',
                estadoActiva: 'true'
            };

            $scope.okModal = function () {

                adminService.savePreguntaFrecuente($scope.preguntaFrecuente)
                    .success(function () {
                        $modalInstance.close($scope.preguntaFrecuente.pregunta);
                    }).error(function (error) {
                        $scope.mensajeError = error.message;
                    });
            }

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);
})();