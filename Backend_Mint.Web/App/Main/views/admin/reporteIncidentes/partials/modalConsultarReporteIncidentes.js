﻿(function () {
    angular.module('app').controller('modalConsultarReporteIncidentesController', ['$scope', '$modalInstance', 'reporteEditar', 'abp.services.app.admin',
        function ($scope, $modalInstance, reporteEditar, adminService) {

            $scope.reporteIncidente = '';
            $scope.map = '';
            $scope.marker = '';
            adminService.getReporteIncidentes({ id: reporteEditar })
                .success(function (data) {
                    $scope.reporteIncidente = data;
                    $scope.map = { center: { latitude: data.latitud, longitude: data.longitud }, zoom: 13 };
                    $scope.marker = {
                        id: 0,
                        coords: {
                            latitude: data.latitud,
                            longitude: data.longitud
                        },
                        options: { draggable: false }
                    };
                });

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
            }


        }]);
})();