﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',
        'abp',
        'ngFileUpload',
        'uiGmapgoogle-maps'
    ]);

    //Configuración de parámetros de paginación por defecto
    app.run(function (paginationConfig) {
        paginationConfig.maxSize = 5;
        paginationConfig.rotate = false;
        paginationConfig.boundaryLinks = true;
        paginationConfig.firstText = "<<";
        paginationConfig.previousText = "<";
        paginationConfig.nextText = ">";
        paginationConfig.lastText = ">>";
    });

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home' //Matches to name of 'Home' menu in Backend_MintNavigationProvider
                })
                .state('PreguntasFrecuentes', {
                    url: '/admin/preguntasFrecuentes',
                    templateUrl: '/App/Main/views/admin/preguntasFrecuentes/preguntasFrecuentes.cshtml',
                    menu: 'PreguntasFrecuentes'
                })
                .state('Noticias', {
                    url: '/admin/noticias',
                    templateUrl: '/App/Main/views/admin/noticias/noticias.cshtml',
                    menu: 'Noticias' 
                })
                .state('HistoriasViales', {
                    url: '/admin/historiasViales',
                    templateUrl: '/App/Main/views/admin/historiasViales/historiasViales.cshtml',
                    menu: 'HistoriasViales'
                })
                .state('Deslizador', {
                    url: '/admin/deslizador',
                    templateUrl: '/App/Main/views/admin/deslizador/deslizador.cshtml',
                    menu: 'Deslizador'
                })

                .state('Categorias', {
                    url: '/admin/categorias',
                    templateUrl: '/App/Main/views/admin/categorias/categorias.cshtml',
                    menu: 'Categorias'
                })
                .state('Diagnostico', {
                    url: '/admin/diagnostico',
                    templateUrl: '/App/Main/views/admin/diagnostico/diagnostico.cshtml',
                    menu: 'Diagnostico'
                })
                .state('ReporteIncidentes', {
                    url: '/admin/reporteIncidentes',
                    templateUrl: '/App/Main/views/admin/reporteIncidentes/reporteIncidentes.cshtml',
                    menu: 'ReporteIncidentes'
                })
                .state('ReporteCalificaciones', {
                    url: '/admin/reporteCalificaciones',
                    templateUrl: '/App/Main/views/admin/calificacionConductores/calificacionConductores.cshtml',
                    menu: 'ReporteCalificaciones'
                })
            ;
        }
    ]);
})();