﻿using Abp.Application.Navigation;
using Abp.Localization;

namespace Backend_Mint.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class Backend_MintNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "Administracion",
                        new LocalizableString("Administracion", Backend_MintConsts.LocalizationSourceName),
                        icon: "fa fa-cogs"
                        ).AddItem(
                            new MenuItemDefinition(
                                "PreguntasFrecuentes",
                                new LocalizableString("PreguntasFrecuentes", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/preguntasFrecuentes",
                                icon: "fa fa-question-circle"
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                "Noticias",
                                new LocalizableString("Noticias", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/noticias",
                                icon: "fa fa-newspaper-o"
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                "HistoriasViales",
                                new LocalizableString("HistoriasViales", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/historiasViales",
                                icon: "fa fa-users"
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                "Deslizador",
                                new LocalizableString("Deslizador", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/deslizador",
                                icon: "fa fa-picture-o"
                            )
                        )
            ).AddItem(
                    new MenuItemDefinition(
                        "Configuracion",
                        new LocalizableString("Configuracion", Backend_MintConsts.LocalizationSourceName),
                        icon: "fa fa-cog"
                        ).AddItem(
                            new MenuItemDefinition(
                                "Categorias",
                                new LocalizableString("Categorias", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/categorias",
                                icon: "fa fa-list-ul"
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                "Diagnostico",
                                new LocalizableString("Diagnostico", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/diagnostico",
                                icon: "fa fa-check-square-o"
                            )
                        )
            ).AddItem(
                    new MenuItemDefinition(
                        "Reportes",
                        new LocalizableString("Reportes", Backend_MintConsts.LocalizationSourceName),
                        icon: "fa fa-mobile"
                        ).AddItem(
                            new MenuItemDefinition(
                                "ReporteIncidentes",
                                new LocalizableString("ReporteIncidentes", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/reporteIncidentes",
                                icon: "fa fa-map-marker"
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                "ReporteCalificaciones",
                                new LocalizableString("ReporteCalificaciones", Backend_MintConsts.LocalizationSourceName),
                                url: "#/admin/reporteCalificaciones",
                                icon: "fa fa-thumbs-o-down"
                            )
                        )
            );
        }
    }
}
