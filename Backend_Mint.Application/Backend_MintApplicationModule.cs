﻿using System.Reflection;
using Abp.Modules;
using AutoMapper;
using System.Linq;
using Backend_Mint.Utilities.AutoMapper;
using System;

namespace Backend_Mint
{
    [DependsOn(typeof(Backend_MintCoreModule))]
    public class Backend_MintApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            InitilizeAutoMapper();
        }

        private void InitilizeAutoMapper()
        {
            Mapper.Initialize(x =>
            {
                var profiles = typeof(AutoMapperBaseProfile).Assembly.GetTypes().Where(perfil => perfil.IsSubclassOf(typeof(AutoMapperBaseProfile)));
                foreach (var perfil in profiles)
                {
                    x.AddProfile((AutoMapperBaseProfile)Activator.CreateInstance(perfil));
                }
            });
        }
    }
}
