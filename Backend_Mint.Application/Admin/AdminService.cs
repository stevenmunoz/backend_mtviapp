﻿using System;
using AutoMapper;
using Backend_Mint.Admin.DTOs.OutputModels;
using Backend_Mint.Admin.DTOs.InputModels;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.UI;
using Backend_Mint.Admin.Entities;
using Backend_Mint.Admin.Repositories;

namespace Backend_Mint.Admin
{
    public class AdminService : IAdminService
    {
        private readonly IDiagnosisRepository _diagnosisRepository;
        private readonly IEventReportRepository _eventReportRepository;
        private readonly IFaqRepository _faqRepository;
        private readonly IGradeReportRepository _gradeReportRepository;
        private readonly INewRepository _newRepository;
        private readonly IReportTypeRepository _reportTypeRepository;
        private readonly IRoadStoryRepository _roadStoryRepository;
        private readonly ISliderRepository _sliderRepository;

        public AdminService(IDiagnosisRepository diagnosisRepository,
                            IEventReportRepository eventReportRepository,
                            IFaqRepository faqRepository,
                            IGradeReportRepository gradeReportRepository,
                            INewRepository newRepository,
                            IReportTypeRepository reportTypeRepository,
                            IRoadStoryRepository roadStoryRepository,
                            ISliderRepository sliderRepository)
        {
            _diagnosisRepository = diagnosisRepository;
            _eventReportRepository = eventReportRepository;
            _faqRepository = faqRepository;
            _gradeReportRepository = gradeReportRepository;
            _newRepository = newRepository;
            _reportTypeRepository = reportTypeRepository;
            _roadStoryRepository = roadStoryRepository;
            _sliderRepository = sliderRepository;
        }

        /*********************************************************************************************
         ***********************************  Preguntas Frecuentes  **********************************
         *********************************************************************************************/

        public GetPreguntaFrecuenteOutput GetPreguntaFrecuente(GetPreguntaFrecuenteInput faqInput)
        {
            return Mapper.Map<GetPreguntaFrecuenteOutput>(_faqRepository.Get(faqInput.Id));
        }

        public GetAllPreguntasFrecuentesOutput GetAllPreguntasFrecuentes()
        {
            var listaFaqs = _faqRepository.GetAllList().OrderBy(p => p.Pregunta);
            return new GetAllPreguntasFrecuentesOutput { PreguntasFrecuentes = Mapper.Map<List<PreguntaFrecuenteOutput>>(listaFaqs) };
        }

        public GetAllPreguntasFrecuentesActivasOutput GetAllPreguntasFrecuentesActivas()
        {
            var listaFaqs = _faqRepository.GetAll().Where(p => p.EsActiva).OrderBy(p => p.Pregunta).ToList();
            return new GetAllPreguntasFrecuentesActivasOutput { PreguntasFrecuentes = Mapper.Map<List<PreguntaFrecuenteOutput>>(listaFaqs) };
        }

        public void SavePreguntaFrecuente(SavePreguntaFrecuenteInput nuevaFaq)
        {
            var existeFaq = _faqRepository.FirstOrDefault(p => p.Pregunta.ToLower() == nuevaFaq.Pregunta.ToLower());

            if (existeFaq == null)
            {
                var preguntaFrecuente = Mapper.Map<Faq>(nuevaFaq);
                preguntaFrecuente.EsActiva = true;
                preguntaFrecuente.FechaPublicacion = DateTime.Now;
                preguntaFrecuente.TenantId = Backend_MintConsts.TenantIdMinT;
                _faqRepository.Insert(preguntaFrecuente);
            }
            else
            {
                var mensajeError = "Ya existe la pregunta frecuente";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void DeletePreguntaFrecuente(DeletePreguntaFrecuenteInput faqEliminar)
        {
            _faqRepository.Delete(faqEliminar.Id);
        }

        public void UpdatePreguntaFrecuente(UpdatePreguntaFrecuenteInput faqUpdate)
        {
            var existeFaq = _faqRepository.FirstOrDefault(p => p.Pregunta.ToLower() == faqUpdate.Pregunta.ToLower() && p.Id != faqUpdate.Id);

            if (existeFaq == null)
            {
                var preguntaFrecuente = _faqRepository.Get(faqUpdate.Id);
                Mapper.Map(faqUpdate, preguntaFrecuente);
                _faqRepository.Update(preguntaFrecuente);
            }
            else
            {
                var mensajeError = "Ya existe la pregunta frecuente.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        /*********************************************************************************************
         ***********************************  Reporte de Incidentes  *********************************
         *********************************************************************************************/

        public GetAllTiposReporteOutput GetAllTiposReporte()
        {
            var listaTiposReporte = _reportTypeRepository.GetAll().Where(t => t.TipoCategoria.Equals(Backend_MintConsts.CategoriaReporte)).ToList().OrderBy(p => p.Nombre);
            return new GetAllTiposReporteOutput { TiposReporte = Mapper.Map<List<TipoReporteOutput>>(listaTiposReporte) };
        }

        public GetAllTiposReporteOutput GetAllTiposVehiculo()
        {
            var listaTiposVehiculo = _reportTypeRepository.GetAll().Where(t => t.TipoCategoria.Equals(Backend_MintConsts.CategoriaVehiculo)).ToList().OrderBy(p => p.Nombre);
            return new GetAllTiposReporteOutput { TiposReporte = Mapper.Map<List<TipoReporteOutput>>(listaTiposVehiculo) };
        }

        public GetAllCategoriasOutput GetAllCategorias()
        {
            var listaCategorias = _reportTypeRepository.GetAll().Where(t => t.TipoCategoria.Equals(Backend_MintConsts.CategoriaHistoria)).ToList().OrderBy(p => p.Nombre);
            return new GetAllCategoriasOutput { TiposReporte = Mapper.Map<List<TipoReporteOutput>>(listaCategorias) };
        }

        public GetAllTiposReporteOutput GetAllTipos()
        {
            var listaTiposReporte = _reportTypeRepository.GetAllList().OrderBy(p => p.TipoCategoria);
            return new GetAllTiposReporteOutput { TiposReporte = Mapper.Map<List<TipoReporteOutput>>(listaTiposReporte) };
        }

        public GetTipoReporteOutput GetTipoReporte(GetTipoReporteInput categoriaInput)
        {
            return Mapper.Map<GetTipoReporteOutput>(_reportTypeRepository.Get(categoriaInput.Id));
        }

        public void SaveTipo(SaveTipoInput nuevoTipo)
        {
            var existeTipo = _reportTypeRepository.FirstOrDefault(p => p.Nombre.ToLower() == nuevoTipo.Nombre.ToLower() && p.TipoCategoria.ToLower() == nuevoTipo.TipoCategoria.ToLower());

            if (existeTipo == null)
            {
                var tipo = Mapper.Map<ReportType>(nuevoTipo);
                tipo.TenantId = Backend_MintConsts.TenantIdMinT;
                _reportTypeRepository.Insert(tipo);
            }
            else
            {
                var mensajeError = "Ya existe el tipo o categoría.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void UpdateTipo(UpdateTipoInput tipoUpdate)
        {
            var existeTipo = _reportTypeRepository.FirstOrDefault(p => p.Nombre.ToLower() == tipoUpdate.Nombre.ToLower() && p.TipoCategoria.ToLower().Equals(tipoUpdate.TipoCategoria.ToLower()) && p.Id != tipoUpdate.Id);
            string mensajeError;

            if (existeTipo == null)
            {
                var tipo = _reportTypeRepository.Get(tipoUpdate.Id);
                var imagenAnterior = tipo.UrlImagen;
                Mapper.Map(tipoUpdate, tipo);
                _reportTypeRepository.Update(tipo);

                //  Eliminamos la imagen anterior
                try
                {
                    if (!imagenAnterior.Equals(tipo.UrlImagen))
                    {
                        string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + imagenAnterior;
                        File.Delete(rutaCompletaArchivo);
                    }
                }
                catch (FileNotFoundException e)
                {
                    mensajeError = "El archivo anterior no se encuentra.";
                    throw new UserFriendlyException(mensajeError);
                }
                catch (Exception e)
                {
                    mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                    throw new UserFriendlyException(mensajeError);
                }
            }
            else
            {
                mensajeError = "Ya existe el tipo o categoría.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void DeleteTipo(DeleteTipoInput tipoEliminar)
        {
            var tipo = _reportTypeRepository.Get(tipoEliminar.Id);
            _reportTypeRepository.Delete(tipoEliminar.Id);
            var mensajeError = "";

            //  Eliminamos la imagen
            try
            {
                string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + tipo.UrlImagen;
                File.Delete(rutaCompletaArchivo);
            }
            catch (FileNotFoundException e)
            {
                mensajeError = "El archivo anterior no se encuentra.";
                throw new UserFriendlyException(mensajeError);
            }
            catch (Exception e)
            {
                mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        /*********************************************************************************************
        ************************************  Reporte de Incidentes  *********************************
        *********************************************************************************************/

        public GetReporteIncidentesOutput GetReporteIncidentes(GetReporteIncidentesInput reporteInput)
        {
            var reporte = Mapper.Map<GetReporteIncidentesOutput>(_eventReportRepository.Get(reporteInput.Id));
            var tipoReporte = _reportTypeRepository.Get(reporte.TipoReporteId);
            reporte.TipoReporteIncidente = tipoReporte.Nombre;
            reporte.TipoReporteImagen = tipoReporte.UrlImagen;
            return reporte;
        }

        public GetAllReporteIncidentesOutput GetAllReporteIncidentes()
        {
            var listaReporteIncidentes = _eventReportRepository.GetAllList().OrderBy(r => r.TipoReporteId);
            var listaResultado = Mapper.Map<List<ReporteIncidenteOutput>>(listaReporteIncidentes);
            foreach (var item in listaResultado)
            {
                var tipoReporte = _reportTypeRepository.Get(item.TipoReporteId);
                item.TipoReporteIncidente = tipoReporte.Nombre;
                item.TipoReporteImagen = tipoReporte.UrlImagen;
            }
            return new GetAllReporteIncidentesOutput { ReportesIncidentes = listaResultado };
        }

        public GetAllRiesgosCercanosOutput GetAllRiesgosCercanos(GetAllRiesgosCercanosInput riesgosCercanosInput)
        {
            var listaReporteIncidentes = _eventReportRepository.GetAllRiesgosCercanos(riesgosCercanosInput.LatitudMinima, riesgosCercanosInput.LatitudMaxima, riesgosCercanosInput.LongitudMinima, riesgosCercanosInput.LongitudMaxima);
            var listaResultado = Mapper.Map<List<ReporteIncidenteOutput>>(listaReporteIncidentes);
            foreach (var item in listaResultado)
            {
                var tipoReporte = _reportTypeRepository.Get(item.TipoReporteId);
                item.TipoReporteIncidente = tipoReporte.Nombre;
                item.TipoReporteImagen = tipoReporte.UrlImagenAdicional;
            }
            return new GetAllRiesgosCercanosOutput { ReportesIncidentes = listaResultado };
        }

        public void SaveReporteIncidentes(SaveReporteIncidentesInput nuevoReporte)
        {
            try
            {
                var reporte = Mapper.Map<EventReport>(nuevoReporte);
                reporte.EsActivo = true;
                reporte.TenantId = Backend_MintConsts.TenantIdMinT;
                reporte.Fecha = DateTime.Now;
                var sasd = _eventReportRepository.Insert(reporte);
            } 
            catch(Exception e) {
                var a = 0;
            }
        }

        public void UpdateStateReporteIncidentes(UpdateStateReporteIncidentesInput reporteUpdate)
        {
            var reporte = _eventReportRepository.Get(reporteUpdate.Id);
            reporte.EsActivo = false;
            _eventReportRepository.Update(reporte);
        }

        /*********************************************************************************************
        *********************************  Calificación de Conductores  ******************************
        *********************************************************************************************/

        public GetReporteCalificacionesOutput GetReporteCalificaciones(GetReporteCalificacionesInput reporteInput)
        {
            var reporte = Mapper.Map<GetReporteCalificacionesOutput>(_gradeReportRepository.Get(reporteInput.Id));
            var tipoReporte = _reportTypeRepository.Get(reporte.TipoVehiculoId);
            reporte.TipoVehiculoReporte = tipoReporte.Nombre;
            reporte.TipoReporteImagen = tipoReporte.UrlImagen;
            return reporte;
        }

        public GetAllReportesCalificacionesOutput GetAllReporteCalificaciones()
        {
            var listaReporteCalificaciones = _gradeReportRepository.GetAllList().OrderBy(r => r.TipoVehiculoId);
            var listaResultado = Mapper.Map<List<ReporteCalificacionesOutput>>(listaReporteCalificaciones);
            foreach (var item in listaResultado)
            {
                var tipoReporte = _reportTypeRepository.Get(item.TipoVehiculoId);
                item.TipoVehiculoReporte = tipoReporte.Nombre;
                item.TipoReporteImagen = tipoReporte.UrlImagen;
            }
            return new GetAllReportesCalificacionesOutput { ReportesCalificaciones = listaResultado };
        }

        public void SaveReporteCalificacion(SaveReporteCalificacionInput nuevaCalificacion)
        {
            var reporte = Mapper.Map<GradeReport>(nuevaCalificacion);
            reporte.EsActiva = true;
            reporte.TenantId = Backend_MintConsts.TenantIdMinT;
            reporte.Fecha = DateTime.Now;
            _gradeReportRepository.Insert(reporte);
        }

        public void UpdateStateCalificacionIncidentes(UpdateStateCalificacionIncidentesInput reporteUpdate)
        {
            var reporte = _gradeReportRepository.Get(reporteUpdate.Id);
            reporte.EsActiva = false;
            _gradeReportRepository.Update(reporte);
        }

        /*********************************************************************************************
        ******************************************  Deslizador  ***************************************
        *********************************************************************************************/

        public GetAllDeslizadorOutput GetAllDeslizador()
        {
            var listaDeslizador = _sliderRepository.GetAllList().OrderBy(p => p.Nombre);
            return new GetAllDeslizadorOutput { Deslizador = Mapper.Map<List<DeslizadorOutput>>(listaDeslizador) };
        }

        public GetDeslizadorOutput GetDeslizador(GetDeslizadorInput deslizadorInput)
        {
            return Mapper.Map<GetDeslizadorOutput>(_sliderRepository.Get(deslizadorInput.Id));
        }

        public void SaveDeslizador(SaveDeslizadorInput nuevaDeslizador)
        {
            var existeDeslizador = _sliderRepository.FirstOrDefault(p => p.Nombre.ToLower() == nuevaDeslizador.Nombre.ToLower());

            if (existeDeslizador == null)
            {
                var deslizador = Mapper.Map<Slider>(nuevaDeslizador);
                deslizador.TenantId = Backend_MintConsts.TenantIdMinT;
                _sliderRepository.Insert(deslizador);
            }
            else
            {
                var mensajeError = "Ya existe la imagen en el slider.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void UpdateDeslizador(UpdateDeslizadorInput deslizadorUpdate)
        {
            var existeDeslizador = _sliderRepository.FirstOrDefault(p => p.Nombre.ToLower() == deslizadorUpdate.Nombre.ToLower() && p.Id != deslizadorUpdate.Id);

            var mensajeError = "";

            if (existeDeslizador == null)
            {
                var deslizador = _sliderRepository.Get(deslizadorUpdate.Id);
                var imagenAnterior = deslizador.UrlImagen;
                Mapper.Map(deslizadorUpdate, deslizador);
                _sliderRepository.Update(deslizador);

                //  Eliminamos la imagen anterior
                try
                {
                    if (!imagenAnterior.Equals(deslizadorUpdate.UrlImagen))
                    {

                        string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + imagenAnterior;
                        File.Delete(rutaCompletaArchivo);
                    }
                }
                catch (FileNotFoundException e)
                {
                    mensajeError = "El archivo anterior no se encuentra.";
                    throw new UserFriendlyException(mensajeError);
                }
                catch (Exception e)
                {
                    mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                    throw new UserFriendlyException(mensajeError);
                }
            }
            else
            {
                mensajeError = "Ya existe la imagen en el slider.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void DeleteDeslizador(DeleteDeslizadorInput deslizadorEliminar)
        {
            var deslizador = _sliderRepository.Get(deslizadorEliminar.Id);
            _sliderRepository.Delete(deslizadorEliminar.Id);
            var mensajeError = "";

            //  Eliminamos la imagen
            try
            {
                string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + deslizador.UrlImagen;
                File.Delete(rutaCompletaArchivo);
            }
            catch (FileNotFoundException e)
            {
                mensajeError = "El archivo anterior no se encuentra.";
                //throw new UserFriendlyException(mensajeError);
            }
            catch (Exception e)
            {
                mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                //throw new UserFriendlyException(mensajeError);
            }
        }

        /*********************************************************************************************
         ******************************************  Noticias  ***************************************
         *********************************************************************************************/

        public GetAllNoticiasOutput GetAllNoticias()
        {
            var listaNoticias = _newRepository.GetAllList().OrderBy(p => p.Fecha);
            return new GetAllNoticiasOutput { Noticias = Mapper.Map<List<NoticiasOutput>>(listaNoticias) };
        }

        public GetAllNoticiasActivasOutput GetAllNoticiasActivas()
        {
            var listaNoticias = _newRepository.GetAll().Where(n => n.EsActiva).ToList().OrderBy(p => p.Fecha);
            return new GetAllNoticiasActivasOutput { Noticias = Mapper.Map<List<NoticiasOutput>>(listaNoticias) };
        }

        public GetNoticiasOutput GetNoticias(GetNoticiasInput noticiasInput)
        {
            return Mapper.Map<GetNoticiasOutput>(_newRepository.Get(noticiasInput.Id));
        }

        public void SaveNoticias(SaveNoticiasInput nuevaNoticias)
        {
            var existeNoticia = _newRepository.FirstOrDefault(p => p.Titulo.ToLower() == nuevaNoticias.Titulo.ToLower());

            if (existeNoticia == null)
            {
                var noticia = Mapper.Map<New>(nuevaNoticias);
                noticia.EsActiva = true;
                noticia.Fecha = DateTime.Now;
                noticia.TenantId = Backend_MintConsts.TenantIdMinT;
                _newRepository.Insert(noticia);
            }
            else
            {
                var mensajeError = "Ya existe la noticia.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void UpdateNoticias(UpdateNoticiasInput noticiaUpdate)
        {
            var existeNoticia = _newRepository.FirstOrDefault(p => p.Titulo.ToLower() == noticiaUpdate.Titulo.ToLower() && p.Id != noticiaUpdate.Id);
            var mensajeError = "";

            if (existeNoticia == null)
            {
                var noticia = _newRepository.Get(noticiaUpdate.Id);
                var imagenAnterior = noticia.UrlImagen;
                Mapper.Map(noticiaUpdate, noticia);
                _newRepository.Update(noticia);

                //  Eliminamos la imagen anterior
                try
                {
                    if (imagenAnterior != noticiaUpdate.URLImagen && imagenAnterior != null)
                    {
                        string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + imagenAnterior;
                        File.Delete(rutaCompletaArchivo);
                    }
                }
                catch (FileNotFoundException e)
                {
                    mensajeError = "El archivo anterior no se encuentra.";
                    throw new UserFriendlyException(mensajeError);
                }
                catch (Exception e)
                {
                    mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                    throw new UserFriendlyException(mensajeError);
                }
            }
            else
            {
                mensajeError = "Ya existe la noticia.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void DeleteNoticias(DeleteNoticiasInput noticiaEliminar)
        {
            var noticia = _newRepository.Get(noticiaEliminar.Id);
            _newRepository.Delete(noticiaEliminar.Id);
            var mensajeError = "";

            //  Eliminamos la imagen
            try
            {
                string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + noticia.UrlImagen;
                File.Delete(rutaCompletaArchivo);
            }
            catch (FileNotFoundException e)
            {
                mensajeError = "El archivo anterior no se encuentra.";
                //throw new UserFriendlyException(mensajeError);
            }
            catch (Exception e)
            {
                mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                //throw new UserFriendlyException(mensajeError);
            }
        }

        /*********************************************************************************************
         ***************************************  Historia Vial  *************************************
         *********************************************************************************************/

        public GetAllHistoriasVialesOutput GetAllHistoriasViales()
        {
            var listaHistorias = _roadStoryRepository.GetAllList();
            var listaResultado = Mapper.Map<List<HistoriaVialOutput>>(listaHistorias);
            foreach (var item in listaResultado)
            {
                var tipoReporte = _reportTypeRepository.Get(item.CategoriaId);
                item.CategoriaNombre = tipoReporte.Nombre;
                item.CategoriaImage = tipoReporte.UrlImagen;
            }
            return new GetAllHistoriasVialesOutput { HistoriasViales = listaResultado };
        }

        public GetAllHistoriasVialesActivasOutput GetAllHistoriasVialesActivas()
        {
            var listaHistorias = _roadStoryRepository.GetAll().Where(h => h.EsActiva).ToList();
            var listaResultado = Mapper.Map<List<HistoriaVialOutput>>(listaHistorias);
            foreach (var item in listaResultado)
            {
                var tipoReporte = _reportTypeRepository.Get(item.CategoriaId);
                item.CategoriaNombre = tipoReporte.Nombre;
                item.CategoriaImage = tipoReporte.UrlImagen;
            }
            return new GetAllHistoriasVialesActivasOutput { HistoriasViales = listaResultado };
        }

        public GetHistoriaVialOutput GetHistoriaVial(GetHistoriaVialInput historiaInput)
        {
            var historiaVial = Mapper.Map<GetHistoriaVialOutput>(_roadStoryRepository.Get(historiaInput.Id));
            historiaVial.Categoria = _reportTypeRepository.Get(historiaVial.CategoriaId).Nombre;
            return historiaVial;
        }

        public void SaveHistoriasVial(SaveHistoriasVialInput nuevaHistoria)
        {
            var existeHistoria = _roadStoryRepository.FirstOrDefault(p => p.Nombre.ToLower() == nuevaHistoria.Nombre.ToLower());

            if (existeHistoria == null)
            {
                var historia = Mapper.Map<RoadStory>(nuevaHistoria);
                historia.EsActiva = false;
                historia.FechaPublicacion = DateTime.Now;
                historia.TenantId = Backend_MintConsts.TenantIdMinT;
                _roadStoryRepository.Insert(historia);
            }
            else
            {
                var mensajeError = "Ya existe la historial vial.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void UpdateHistoriasVial(UpdateHistoriasVialInput historiaUpdate)
        {
            var existeHistoria = _roadStoryRepository.FirstOrDefault(p => p.Nombre.ToLower() == historiaUpdate.Nombre.ToLower() && p.Id != historiaUpdate.Id);

            if (existeHistoria == null)
            {
                var historia = _roadStoryRepository.Get(historiaUpdate.Id);
                Mapper.Map(historiaUpdate, historia);
                _roadStoryRepository.Update(historia);
            }
            else
            {
                var mensajeError = "Ya existe la historial vial.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void DeleteHistoriasVial(DeleteHistoriasVialInput historiaEliminar)
        {
            _roadStoryRepository.Delete(historiaEliminar.Id);
        }

        ///*********************************************************************************************
        // *************************************  Diagnostico Vial  ************************************
        // *********************************************************************************************/

        public GetAllItemsByDiagnosticoVialOutput GetAllItemsDiagnosticoVial()
        {
            var listaItemsDiagnostico = _diagnosisRepository.GetAllList().OrderBy(p => p.Id);
            return new GetAllItemsByDiagnosticoVialOutput { ItemsDiagnosticoVial = Mapper.Map<List<ItemByDiagnosticoVialOutput>>(listaItemsDiagnostico) };
        }

        public GetAllItemsActivosByDiagnosticoVialOutput GetAllItemsActivosByDiagnosticoVial()
        {
            var listaItemsDiagnostico = _diagnosisRepository.GetAll().Where(i => i.EsActivo).ToList().OrderBy(p => p.Id);
            return new GetAllItemsActivosByDiagnosticoVialOutput { ItemsDiagnosticoVial = Mapper.Map<List<ItemByDiagnosticoVialOutput>>(listaItemsDiagnostico) };
        }

        public GetItemByDiagnosticoVialOutput GetItemByDiagnosticoVial(GetItemByDiagnosticoVialInput itemDiagnosticoInput)
        {
            return Mapper.Map<GetItemByDiagnosticoVialOutput>(_diagnosisRepository.Get(itemDiagnosticoInput.Id));
        }

        public void SaveItemDiagnosticoVial(SaveItemDiagnosticoVialInput nuevoItem)
        {
            var existeItemDiagnostico = _diagnosisRepository.FirstOrDefault(p => p.Nombre.ToLower() == nuevoItem.Nombre.ToLower());

            if (existeItemDiagnostico == null)
            {
                var itemDiagnostico = Mapper.Map<Diagnosis>(nuevoItem);
                itemDiagnostico.EsActivo = true;
                itemDiagnostico.TenantId = Backend_MintConsts.TenantIdMinT;
                _diagnosisRepository.Insert(itemDiagnostico);
            }
            else
            {
                var mensajeError = "Ya existe el item del diagnóstico vial.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void UpdateItemDiagnosticoVial(UpdateItemDiagnosticoVialInput itemDiagnosticoUpdate)
        {
            var existeItemDiagnostico = _diagnosisRepository.FirstOrDefault(p => p.Nombre.ToLower() == itemDiagnosticoUpdate.Nombre.ToLower() && p.Id != itemDiagnosticoUpdate.Id);
            var mensajeError = "";

            if (existeItemDiagnostico == null)
            {
                var itemDiagnostico = _diagnosisRepository.Get(itemDiagnosticoUpdate.Id);
                var imagenAnterior = itemDiagnostico.UrlImagen;
                Mapper.Map(itemDiagnosticoUpdate, itemDiagnostico);
                _diagnosisRepository.Update(itemDiagnostico);

                //  Eliminamos la imagen anterior
                try
                {
                    if (!imagenAnterior.Equals(itemDiagnosticoUpdate.UrlImagen))
                    {
                        string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + imagenAnterior;
                        File.Delete(rutaCompletaArchivo);
                    }
                }
                catch (FileNotFoundException e)
                {
                    mensajeError = "El archivo anterior no se encuentra.";
                    throw new UserFriendlyException(mensajeError);
                }
                catch (Exception e)
                {
                    mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                    throw new UserFriendlyException(mensajeError);
                }
            }
            else
            {
                mensajeError = "Ya existe el item del diagnóstico vial.";
                throw new UserFriendlyException(mensajeError);
            }
        }

        public void DeleteItemDiagnosticoVial(DeleteItemDiagnosticoVialInput itemDiagnosticoEliminar)
        {
            var item = _diagnosisRepository.Get(itemDiagnosticoEliminar.Id);
            _diagnosisRepository.Delete(itemDiagnosticoEliminar.Id);
            var mensajeError = "";

            //  Eliminamos la imagen
            try
            {
                string rutaCompletaArchivo = System.AppDomain.CurrentDomain.BaseDirectory + item.UrlImagen;
                File.Delete(rutaCompletaArchivo);
            }
            catch (FileNotFoundException e)
            {
                mensajeError = "El archivo anterior no se encuentra.";
                //throw new UserFriendlyException(mensajeError);
            }
            catch (Exception e)
            {
                mensajeError = "Ocurrió un problema al eliminar el archivo anterior.";
                //throw new UserFriendlyException(mensajeError);
            }
        }
    }
}