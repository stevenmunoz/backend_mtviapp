﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.DTOs.InputModels
{
    public class GetAllRiesgosCercanosInput : IInputDto
    {
        [Required]
        public decimal LatitudMinima { get; set; }
        [Required]
        public decimal LatitudMaxima { get; set; }
        [Required]
        public decimal LongitudMinima { get; set; }
        [Required]
        public decimal LongitudMaxima { get; set; }
    }
}
