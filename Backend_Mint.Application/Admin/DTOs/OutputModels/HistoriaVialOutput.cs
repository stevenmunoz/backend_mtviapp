﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.DTOs.OutputModels
{
    public class HistoriaVialOutput : EntityDto, IOutputDto
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string NombrePersona { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public string Url { get; set; }
        public bool EsActiva { get; set; }
        public int CategoriaId { get; set; }
        public string CategoriaNombre { get; set; }
        public string CategoriaImage { get; set; }
    }
}
