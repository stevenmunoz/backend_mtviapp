﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.DTOs.OutputModels
{
    public class ReporteIncidenteOutput : EntityDto, IOutputDto
    {
        public int TipoReporteId { get; set; }
        public string TipoReporteIncidente { get; set; }
        public string TipoReporteImagen { get; set; }
        public string TipoRiesgosImagen { get; set; }
        public string Direccion { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public string Distancia { get; set; }
        public string Observaciones { get; set; }
        public DateTime Fecha { get; set; }
        public bool EsActivo { get; set; }
        public string LatitudString { get; set; }
        public string LongitudString { get; set; }
    }
}
