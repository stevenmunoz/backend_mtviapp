﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.DTOs.OutputModels
{
    public class GetAllHistoriasVialesOutput : IOutputDto
    {
        public List<HistoriaVialOutput> HistoriasViales { get; set; }
    }
}
