﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.DTOs.OutputModels
{
    public class GetAllTiposRiesgosOutput : IOutputDto
    {
        public List<TipoReporteOutput> TiposRiesgos { get; set; }
    }
}
