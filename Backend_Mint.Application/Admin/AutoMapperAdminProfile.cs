﻿using Backend_Mint.Admin.DTOs.InputModels;
using Backend_Mint.Admin.DTOs.OutputModels;
using Backend_Mint.Admin.Entities;
using Backend_Mint.Utilities.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin
{
    public class AutoMapperAdminProfile : AutoMapperBaseProfile
    {
        public AutoMapperAdminProfile ()
            : base ("AutoMapperAdminProfile")
        {

        }

        protected override void CreateMappings()
        {
            //  Preguntas Frecuentes
            CreateMap<SavePreguntaFrecuenteInput, Faq>();
            CreateMap<UpdatePreguntaFrecuenteInput, Faq>();
            CreateMap<Faq, GetPreguntaFrecuenteOutput>();
            CreateMap<Faq, PreguntaFrecuenteOutput>();

            //  Reporte Incidentes

            CreateMap<SaveTipoInput, ReportType>();
            CreateMap<UpdateTipoInput, ReportType>();
            CreateMap<ReportType, GetTipoReporteOutput>();
            CreateMap<ReportType, TipoReporteOutput>();

            //  Reporte de Incidentes

            CreateMap<EventReport, GetReporteIncidentesOutput>();
            CreateMap<EventReport, ReporteIncidenteOutput>()
                .ForMember(dest => dest.LatitudString, opt => opt.MapFrom(src => src.Latitud.ToString().Replace(".", ",")))
                .ForMember(dest => dest.LongitudString, opt => opt.MapFrom(src => src.Longitud.ToString().Replace(".", ",")));
            CreateMap<SaveReporteIncidentesInput, EventReport>();

            //  Reporte de Calificaciones

            CreateMap<GradeReport, GetReporteCalificacionesOutput>();
            CreateMap<GradeReport, ReporteCalificacionesOutput>();
            CreateMap<SaveReporteCalificacionInput, GradeReport>();

            //  Noticias
            CreateMap<SaveNoticiasInput, New>();
            CreateMap<UpdateNoticiasInput, New>();
            CreateMap<New, GetNoticiasOutput>();
            CreateMap<New, NoticiasOutput>();

            //  Deslizador
            CreateMap<SaveDeslizadorInput, Slider>();
            CreateMap<UpdateDeslizadorInput, Slider>();
            CreateMap<Slider, GetDeslizadorOutput>();
            CreateMap<Slider, DeslizadorOutput>();

            //  Historia Vial
            CreateMap<SaveHistoriasVialInput, RoadStory>();
            CreateMap<UpdateHistoriasVialInput, RoadStory>();
            CreateMap<RoadStory, GetHistoriaVialOutput>();
            CreateMap<RoadStory, HistoriaVialOutput>();

            //  Diagnostico Vial
            CreateMap<SaveItemDiagnosticoVialInput, Diagnosis>();
            CreateMap<UpdateItemDiagnosticoVialInput, Diagnosis>();
            CreateMap<Diagnosis, GetItemByDiagnosticoVialOutput>();
            CreateMap<Diagnosis, ItemByDiagnosticoVialOutput>();
        }
    }
}
