﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.UI;
using Backend_Mint.Users.Repositories;
using Backend_Mint.Users.DTOs.OutputModels;
using Backend_Mint.Users.DTOs.InputModels;

namespace Backend_Mint.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /*********************************************************************************************
         *************************************  Login de Usuario  ************************************
         *********************************************************************************************/

        public GetLoginUserOutput GetLoginUser(GetLoginUserInput loginUser)
        {
            _userRepository.ChangeStringConection(loginUser.User, loginUser.Password);
            var login = _userRepository.GetUserLogin("admin", "123qwe");
            if (login != null)
            {
                return new GetLoginUserOutput() { Result = true };
            }
            return new GetLoginUserOutput() { Result = false };
        }

    }
}