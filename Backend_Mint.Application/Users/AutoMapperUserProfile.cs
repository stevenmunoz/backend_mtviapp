﻿using Backend_Mint.Users.DTOs.OutputModels;
using Backend_Mint.Users.Entities;
using Backend_Mint.Utilities.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Users
{
    public class AutoMapperUserProfile : AutoMapperBaseProfile
    {
        public AutoMapperUserProfile()
            : base("AutoMapperUserProfile")
        {

        }

        protected override void CreateMappings()
        {
            //  Login de Usuario

            CreateMap<User, GetLoginUserOutput>();

        }
    }
}