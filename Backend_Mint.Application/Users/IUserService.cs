﻿using Abp.Application.Services;
using Backend_Mint.Users.DTOs.InputModels;
using Backend_Mint.Users.DTOs.OutputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Users
{
    public interface IUserService : IApplicationService
    {
        // Inicio de Sesión

        GetLoginUserOutput GetLoginUser(GetLoginUserInput loginUser);

    }
}
