﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Users.DTOs.OutputModels
{
    public class GetLoginUserOutput : IOutputDto
    {
        public bool Result { get; set; }
    }
}
