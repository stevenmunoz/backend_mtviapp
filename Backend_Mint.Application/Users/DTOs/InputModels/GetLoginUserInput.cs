﻿
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Users.DTOs.InputModels
{
    public class GetLoginUserInput : IInputDto
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
