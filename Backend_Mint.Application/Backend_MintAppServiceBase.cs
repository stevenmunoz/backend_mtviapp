﻿using Abp.Application.Services;

namespace Backend_Mint
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class Backend_MintAppServiceBase : ApplicationService
    {
        protected Backend_MintAppServiceBase()
        {
            LocalizationSourceName = Backend_MintConsts.LocalizationSourceName;
        }
    }
}