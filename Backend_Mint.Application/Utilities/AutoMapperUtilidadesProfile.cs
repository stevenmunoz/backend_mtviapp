﻿using Backend_Mint.Utilities.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Utilities
{
    public class AutoMapperUtilidadesProfile : AutoMapperBaseProfile
    {
        public AutoMapperUtilidadesProfile()
            : base("AutoMapperUtilidadesProfile")
        {
        }

        protected override void CreateMappings()
        {

        }
    }
}
