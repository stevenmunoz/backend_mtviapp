﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Utilities.AutoMapper
{
    public abstract class AutoMapperBaseProfile : Profile
    {
        private readonly string _profileName;

        protected AutoMapperBaseProfile(string profileName)
        {
            _profileName = profileName;
        }

        public override string ProfileName
        {
            get
            {
                return _profileName;
            }
        }

        protected override void Configure()
        {
            CreateMappings();
        }

        protected abstract void CreateMappings();
    }
}
