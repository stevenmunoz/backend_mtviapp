﻿using Abp.Application.Services;
using Backend_Mint.Utilities.DTOs.InputModels;
using Backend_Mint.Utilities.DTOs.OutputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Utilities
{
    public interface IUtilidadesService : IApplicationService
    {
        ReadFileOutput ReadFile(ReadFileInput ruta);

    }
}


