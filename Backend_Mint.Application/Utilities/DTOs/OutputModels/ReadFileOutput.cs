﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Utilities.DTOs.OutputModels
{
    public class ReadFileOutput : IOutputDto
    {
        public string UrlImagen { get; set; }
       
    }
}
