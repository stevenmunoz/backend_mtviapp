﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;

namespace Backend_Mint
{
    [DependsOn(typeof(AbpWebApiModule), typeof(Backend_MintApplicationModule))]
    public class Backend_MintWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(Backend_MintApplicationModule).Assembly, "app")
                .Build();
        }
    }
}
