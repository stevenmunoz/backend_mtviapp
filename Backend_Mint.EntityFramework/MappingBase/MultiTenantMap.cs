﻿using Backend_Mint.BaseEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.MappingBase
{
    public class MultiTenantMap<TMultiTenantEntity> : EntityTypeConfiguration<TMultiTenantEntity>
        where TMultiTenantEntity : class, IMultiTenant
    {
        public MultiTenantMap() : base ()
        {
            //  Primary Key
            HasKey(m => m.Id);
        }
    }
}
