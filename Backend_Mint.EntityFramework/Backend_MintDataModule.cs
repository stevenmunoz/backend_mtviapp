﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using Backend_Mint.EntityFramework;

namespace Backend_Mint
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(Backend_MintCoreModule))]
    public class Backend_MintDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<Backend_MintDbContext>(null);
        }
    }
}
