﻿using Abp.EntityFramework;
using Backend_Mint.EntityFramework;
using Backend_Mint.EntityFramework.Repositories;
using Backend_Mint.Users.Entities;
using Backend_Mint.Users.Repositories;
using Encrypt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Users.Repositories
{
    public class UserRepository : Backend_MintRepositoryBase<User>, IUserRepository
    {
        private readonly Backend_MintDbContext _backendMintDbContext;

        public UserRepository(IDbContextProvider<Backend_MintDbContext> dbContextProvider, Backend_MintDbContext backendMintDbContext)
            : base(dbContextProvider)
        {
            _backendMintDbContext = backendMintDbContext;
        }

        public void ChangeStringConection (string usuario, string contrasena)
        {
            _backendMintDbContext.CreateDynamicConnectionString(usuario, contrasena);
        }

        public User GetUserLogin(string UserName, string Password)
        {
            var passwordEncrypt = EncryptText(Password);
            return GetAll().FirstOrDefault(u => u.UserName == UserName
                                    && u.Password == passwordEncrypt);
        }

        private static string EncryptText(string text)
        {
            return LibraryEncrypt.Encrypt(text, "X81&sP)Wk5j0(Rm=q9Z4s$7Y", "gsC3)h#8kmLy%Z", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256);
        }
    }
}
