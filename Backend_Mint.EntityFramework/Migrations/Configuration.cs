namespace Backend_Mint.Migrations
{
    using Backend_Mint.Admin.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Backend_Mint.EntityFramework.Backend_MintDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Backend_Mint.EntityFramework.Backend_MintDbContext context)
        {
            
        }
    }
}
