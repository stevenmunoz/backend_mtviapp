namespace Backend_Mint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "AMVS.New",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Titulo = c.String(maxLength: 2000),
                        Descripcion = c.String(maxLength: 2000),
                        Fecha = c.DateTime(nullable: false),
                        Url = c.String(maxLength: 2000),
                        UrlImagen = c.String(maxLength: 2000),
                        EsActiva = c.Decimal(nullable: false, precision: 1, scale: 0),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.Diagnosis",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Nombre = c.String(maxLength: 2000),
                        Observaciones = c.String(maxLength: 2000),
                        EsRequerido = c.Decimal(nullable: false, precision: 1, scale: 0),
                        UrlImagen = c.String(maxLength: 2000),
                        EsActivo = c.Decimal(nullable: false, precision: 1, scale: 0),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.EventReport",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Direccion = c.String(maxLength: 2000),
                        Latitud = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitud = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Distancia = c.String(maxLength: 2000),
                        Observaciones = c.String(maxLength: 2000),
                        EsActivo = c.Decimal(nullable: false, precision: 1, scale: 0),
                        TipoReporteId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.Faq",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Pregunta = c.String(maxLength: 2000),
                        Respuesta = c.String(maxLength: 2000),
                        FechaPublicacion = c.DateTime(nullable: false),
                        EsActiva = c.Decimal(nullable: false, precision: 1, scale: 0),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.GradeReport",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Placa = c.String(maxLength: 2000),
                        Empresa = c.String(maxLength: 2000),
                        Observaciones = c.String(maxLength: 2000),
                        Calificacion = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UrlImagen = c.String(maxLength: 2000),
                        EsActiva = c.Decimal(nullable: false, precision: 1, scale: 0),
                        TipoVehiculoId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.ReportType",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Nombre = c.String(maxLength: 2000),
                        TipoCategoria = c.String(maxLength: 2000),
                        UrlImagen = c.String(maxLength: 2000),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.RoadStory",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Nombre = c.String(maxLength: 2000),
                        Descripcion = c.String(maxLength: 2000),
                        NombrePersona = c.String(maxLength: 2000),
                        FechaPublicacion = c.DateTime(nullable: false),
                        Url = c.String(maxLength: 2000),
                        EsActiva = c.Decimal(nullable: false, precision: 1, scale: 0),
                        CategoriaId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AMVS.Slider",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Nombre = c.String(maxLength: 2000),
                        UrlImagen = c.String(maxLength: 2000),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("AMVS.Slider");
            DropTable("AMVS.RoadStory");
            DropTable("AMVS.ReportType");
            DropTable("AMVS.GradeReport");
            DropTable("AMVS.Faq");
            DropTable("AMVS.EventReport");
            DropTable("AMVS.Diagnosis");
            DropTable("AMVS.New");
        }
    }
}
