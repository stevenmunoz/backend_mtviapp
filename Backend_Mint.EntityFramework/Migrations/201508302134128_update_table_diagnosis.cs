namespace Backend_Mint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_table_diagnosis : DbMigration
    {
        public override void Up()
        {
            AddColumn("AMVS.Diagnosis", "EsOpcional", c => c.Decimal(nullable: false, precision: 1, scale: 0));
        }
        
        public override void Down()
        {
            DropColumn("AMVS.Diagnosis", "EsOpcional");
        }
    }
}
