namespace Backend_Mint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_table_user : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "AMVS.User",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 200),
                        Surname = c.String(maxLength: 200),
                        UserName = c.String(maxLength: 200),
                        Password = c.String(maxLength: 1000),
                        PasswordResetCode = c.String(maxLength: 1000),
                        TenantId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("AMVS.User");
        }
    }
}
