namespace Backend_Mint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class set_precision_table_eventReport : DbMigration
    {
        public override void Up()
        {
            AlterColumn("AMVS.EventReport", "Latitud", c => c.Decimal(nullable: false, precision: 19, scale: 16));
            AlterColumn("AMVS.EventReport", "Longitud", c => c.Decimal(nullable: false, precision: 19, scale: 16));
        }
        
        public override void Down()
        {
            AlterColumn("AMVS.EventReport", "Longitud", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("AMVS.EventReport", "Latitud", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
