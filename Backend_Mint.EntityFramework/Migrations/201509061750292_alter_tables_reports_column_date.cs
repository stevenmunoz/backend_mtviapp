namespace Backend_Mint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alter_tables_reports_column_date : DbMigration
    {
        public override void Up()
        {
            AddColumn("AMVS.EventReport", "Fecha", c => c.DateTime(nullable: false));
            AddColumn("AMVS.GradeReport", "Fecha", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("AMVS.GradeReport", "Fecha");
            DropColumn("AMVS.EventReport", "Fecha");
        }
    }
}
