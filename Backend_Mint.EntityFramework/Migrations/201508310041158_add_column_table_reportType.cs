namespace Backend_Mint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_table_reportType : DbMigration
    {
        public override void Up()
        {
            AddColumn("AMVS.ReportType", "UrlImagenAdicional", c => c.String(maxLength: 2000));
        }
        
        public override void Down()
        {
            DropColumn("AMVS.ReportType", "UrlImagenAdicional");
        }
    }
}
