﻿using System.Configuration;
using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Backend_Mint.Admin.Entities;
using Backend_Mint.Admin.Mappings;
using Backend_Mint.Users.Entities;

namespace Backend_Mint.EntityFramework
{
    public class Backend_MintDbContext : AbpDbContext
    {
        //TODO: Define an IDbSet for each Entity...
        public virtual IDbSet<New> News { get; set; }

        //Example:
        //public virtual IDbSet<User> Users { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public Backend_MintDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in Backend_MintDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of Backend_MintDbContext since ABP automatically handles it.
         */
        public Backend_MintDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        public void CreateDynamicConnectionString(string usuario, string pass)
        {
            for (int i = 0; i < ConfigurationManager.ConnectionStrings.Count; i++)
            {
                if (ConfigurationManager.ConnectionStrings[i].ToString().Contains("DATA SOURCE=(DESCRIPTION=(ADDRESS_LIST"))
                {
                    var settings = ConfigurationManager.ConnectionStrings[i];
                    var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                    fi.SetValue(settings, false);
                    var serverConnectionString = ConfigurationManager.AppSettings["serverConnectionString"];
                    settings.ConnectionString = string.Format(serverConnectionString, pass, usuario);
                }
            }
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("AMVS");

            modelBuilder.Entity<Diagnosis>().ToTable("Diagnosis");
            modelBuilder.Entity<EventReport>().ToTable("EventReport");
            modelBuilder.Entity<EventReport>().Property(p => p.Latitud).HasPrecision(19, 16);
            modelBuilder.Entity<EventReport>().Property(p => p.Longitud).HasPrecision(19, 16);
            modelBuilder.Entity<Faq>().ToTable("Faq");
            modelBuilder.Entity<GradeReport>().ToTable("GradeReport");
            modelBuilder.Entity<New>().ToTable("New");
            modelBuilder.Entity<ReportType>().ToTable("ReportType");
            modelBuilder.Entity<RoadStory>().ToTable("RoadStory");
            modelBuilder.Entity<Slider>().ToTable("Slider");
            modelBuilder.Entity<User>().ToTable("User");

            base.OnModelCreating(modelBuilder);
        }
    }
}
