﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace Backend_Mint.EntityFramework.Repositories
{
    public abstract class Backend_MintRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<Backend_MintDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected Backend_MintRepositoryBase(IDbContextProvider<Backend_MintDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class Backend_MintRepositoryBase<TEntity> : Backend_MintRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected Backend_MintRepositoryBase(IDbContextProvider<Backend_MintDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
