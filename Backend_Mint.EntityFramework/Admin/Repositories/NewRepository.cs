﻿using Abp.EntityFramework;
using Backend_Mint.Admin.Entities;
using Backend_Mint.EntityFramework;
using Backend_Mint.EntityFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.Repositories
{
    public class NewRepository : Backend_MintRepositoryBase<New>, INewRepository
    {
        public NewRepository(IDbContextProvider<Backend_MintDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }
    }
}
