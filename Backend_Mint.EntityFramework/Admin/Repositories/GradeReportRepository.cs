﻿using Abp.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Backend_Mint.Admin.Entities;
using Backend_Mint.EntityFramework;
using Backend_Mint.EntityFramework.Repositories;

namespace Backend_Mint.Admin.Repositories
{
    public class GradeReportRepository : Backend_MintRepositoryBase<GradeReport>, IGradeReportRepository
    {
        public GradeReportRepository(IDbContextProvider<Backend_MintDbContext> dbContextProvider)
           : base(dbContextProvider)
        {

        }
    }
}
