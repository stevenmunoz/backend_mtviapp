﻿using Abp.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Backend_Mint.Admin.Entities;
using Backend_Mint.EntityFramework;
using Backend_Mint.EntityFramework.Repositories;

namespace Backend_Mint.Admin.Repositories
{
    public class EventReportRepository : Backend_MintRepositoryBase<EventReport>, IEventReportRepository
    {
        public EventReportRepository(IDbContextProvider<Backend_MintDbContext> dbContextProvider)
           : base(dbContextProvider)
        {

        }

        public List<EventReport> GetAllRiesgosCercanos(decimal latitudMinima, decimal latitudMaxima, decimal longitudMinima, decimal longitudMaxima)
        {
            return GetAll().Where(r => r.EsActivo 
                && r.Latitud >= latitudMinima
                && r.Latitud <= latitudMaxima
                && r.Longitud >= longitudMinima
                && r.Longitud <= longitudMaxima).ToList();
        }
    }
}
