﻿using Abp.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.Admin.Entities;
using Backend_Mint.EntityFramework;
using Backend_Mint.EntityFramework.Repositories;

namespace Backend_Mint.Admin.Repositories
{
    public class FaqRepository : Backend_MintRepositoryBase<Faq>, IFaqRepository
    {
        public FaqRepository(IDbContextProvider<Backend_MintDbContext> dbContextProvider)
           : base(dbContextProvider)
        {

        }
    }
}
