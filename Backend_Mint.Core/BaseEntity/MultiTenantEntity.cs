﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.BaseEntity
{
    public abstract class MultiTenantEntity : Entity, IMultiTenant, IMustHaveTenant
    {
        public int TenantId { get; set; }
    }
}
