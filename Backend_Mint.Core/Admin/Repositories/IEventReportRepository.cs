﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.Admin.Entities;

namespace Backend_Mint.Admin.Repositories
{
    public interface IEventReportRepository : IRepository<EventReport>
    {
        List<EventReport> GetAllRiesgosCercanos(decimal latitudMinima, decimal latitudMaxima, decimal longitudMinima, decimal longitudMaxima);
    }
}
