﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.Admin.Entities;

namespace Backend_Mint.Admin.Repositories
{
    public interface IFaqRepository : IRepository<Faq>
    {
    }
}
