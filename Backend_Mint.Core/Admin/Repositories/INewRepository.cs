﻿using Abp.Domain.Repositories;
using Backend_Mint.Admin.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Admin.Repositories
{
    public interface INewRepository :  IRepository<New>
    {
    }
}
