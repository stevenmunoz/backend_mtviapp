﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.BaseEntity;

namespace Backend_Mint.Admin.Entities
{
    [Table("AMVS.GradeReport")]
    public class GradeReport : Entity
    {
        [MaxLength(2000)]
        public string Placa { get; set; }
        [MaxLength(2000)]
        public string Empresa { get; set; }
        [MaxLength(2000)]
        public string Observaciones { get; set; }
        public decimal Calificacion { get; set; }
        [MaxLength(2000)]
        public string UrlImagen { get; set; }
        public DateTime Fecha { get; set; }
        public bool EsActiva { get; set; }
        public int TipoVehiculoId { get; set; }
        public int TenantId { get; set; }
    }
}
