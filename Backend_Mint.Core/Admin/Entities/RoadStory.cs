﻿
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.BaseEntity;

namespace Backend_Mint.Admin.Entities
{
    [Table("AMVS.RoadStory")]
    public class RoadStory : Entity
    {
        [MaxLength(2000)]
        public string Nombre { get; set; }
        [MaxLength(2000)]
        public string Descripcion { get; set; }
        [MaxLength(2000)]
        public string NombrePersona { get; set; }
        public DateTime FechaPublicacion { get; set; }
        [MaxLength(2000)]
        public string Url { get; set; }
        public bool EsActiva { get; set; }
        public int CategoriaId { get; set; }
        public int TenantId { get; set; }
    }
}
