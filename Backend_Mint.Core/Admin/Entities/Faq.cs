﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.BaseEntity;

namespace Backend_Mint.Admin.Entities
{
    [Table("AMVS.Faq")]
    public class Faq : Entity
    {
        [MaxLength(2000)]
        public string Pregunta { get; set; }
        [MaxLength(2000)]
        public string Respuesta { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public bool EsActiva { get; set; }
        public int TenantId { get; set; }
    }
}
