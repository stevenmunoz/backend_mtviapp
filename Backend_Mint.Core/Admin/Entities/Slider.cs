﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.BaseEntity;

namespace Backend_Mint.Admin.Entities
{
    [Table("AMVS.Slider")]
    public class Slider : Entity
    {
        [MaxLength(2000)]
        public string Nombre { get; set; }
        [MaxLength(2000)]
        public string UrlImagen { get; set; }
        public int TenantId { get; set; }
    }
}
