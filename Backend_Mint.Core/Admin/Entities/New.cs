﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess;
using Oracle.ManagedDataAccess.Client;

namespace Backend_Mint.Admin.Entities
{
    [Table("AMVS.New")]
    public class New : Entity
    {
        [MaxLength(2000)]
        public string Titulo { get; set; }
        [MaxLength(2000)]
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        [MaxLength(2000)]
        public string Url { get; set; }
        [MaxLength(2000)]
        public string UrlImagen { get; set; }
        public bool EsActiva { get; set; }
        public int TenantId { get; set; }
    }
}
