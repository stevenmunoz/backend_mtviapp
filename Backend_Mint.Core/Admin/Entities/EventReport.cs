﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend_Mint.BaseEntity;

namespace Backend_Mint.Admin.Entities
{
    [Table("AMVS.EventReport")]
    public class EventReport : Entity
    {
        [MaxLength(2000)]
        public string Direccion { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        [MaxLength(2000)]
        public string Distancia { get; set; }
        [MaxLength(2000)]
        public string Observaciones { get; set; }
        public DateTime Fecha { get; set; }
        public bool EsActivo { get; set; }
        public int TipoReporteId { get; set; }
        public int TenantId { get; set; }
    }
}
