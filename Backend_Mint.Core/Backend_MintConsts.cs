﻿namespace Backend_Mint
{
    public class Backend_MintConsts
    {
        public const string LocalizationSourceName = "Backend_Mint";

        //  Tenant
        public const int TenantIdMinT = 1;

        public const string CategoriaReporte = "reporte";
        public const string CategoriaVehiculo = "vehiculo";
        public const string CategoriaHistoria = "historia";
    }
}