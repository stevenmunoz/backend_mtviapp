﻿using System.Reflection;
using Abp.Modules;

namespace Backend_Mint
{
    public class Backend_MintCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
