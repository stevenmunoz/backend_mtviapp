﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess;
using Oracle.ManagedDataAccess.Client;

namespace Backend_Mint.Users.Entities
{
    [Table("AMVS.User")]
    public class User : Entity
    {
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string Surname { get; set; }
        [MaxLength(200)]
        public string UserName { get; set; }
        [MaxLength(1000)]
        public string Password { get; set; }
        [MaxLength(1000)]
        public string PasswordResetCode { get; set; }
        public int TenantId { get; set; }
    }
}
