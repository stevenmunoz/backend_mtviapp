﻿using Abp.Domain.Repositories;
using Backend_Mint.Users.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend_Mint.Users.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        void ChangeStringConection(string usuario, string contrasena);
        User GetUserLogin(string UserName, string Password);
    }
}
